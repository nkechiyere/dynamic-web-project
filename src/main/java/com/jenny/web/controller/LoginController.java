package com.jenny.web.controller;

import com.jenny.web.model.Login;
import com.jenny.web.service.FormValidator;
import com.jenny.web.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class LoginController {


    private static final String MODEL_REQUEST = "request";
    private static Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private FormValidator validator;

    @Autowired
    private LoginService loginService;



    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String showLoginPage (Model model) {

        LOGGER.info ("navigated inside login page");
        model.addAttribute("request", new Login());
        return "login";

    }


    @RequestMapping (method = RequestMethod.POST, value = "/login")
    public String readFromDatabase (@ModelAttribute(MODEL_REQUEST) Login login, BindingResult result, Model model) {

        LOGGER.info ("About to validate login details");

        validator.validateField(login, result, "userName");
        validator.validateField(login, result, "password");

        if (result.hasErrors()) {
            LOGGER.error("{} field is empty", model.addAttribute(model));
            return "login";
        }

        LOGGER.info ("about to read database - login table");

        String getUserName  = login.getUserName();
        LOGGER.info ("entered user name {}", getUserName);

        List<Login> loginInformation = loginService.searchByUserName(getUserName);
        LOGGER.info ("login information retrieved, {}", loginInformation);

        if (!((loginInformation == null) || (loginInformation.isEmpty()))){

            model.addAttribute("loginInformation", loginInformation);
            return "index";
        }

        else {

            LOGGER.error ("no match in the database");
            return "login";
        }
    }





}
