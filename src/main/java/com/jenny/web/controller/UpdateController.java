package com.jenny.web.controller;

import com.jenny.web.model.Article;
import com.jenny.web.repository.ArticleRepository;
import com.jenny.web.service.ArticleService;
import com.jenny.web.service.FormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class UpdateController {

    private static final String MODEL_REQUEST = "request";
    private static Logger LOGGER = LoggerFactory.getLogger(UpdateController.class);

    @Autowired
    private ArticleService articleService;

    @Autowired
    private FormValidator validator;

    @Autowired
    private ArticleRepository articleRepository;



    @RequestMapping (method = RequestMethod.GET, value = "/update")
    public String shoUpdatePage(Model model) {

        LOGGER.info ("navigated inside edit service page");

        model.addAttribute("request", new Article());
        return "update";
    }




    //@RequestMapping to add new webpage content

    @RequestMapping(method = RequestMethod.GET, value = "/insert-new-service")
    public String showInsertNewServicePage (Model model) {

        LOGGER.info ("navigated inside insert new service page");

        model.addAttribute("request", new Article());
        return "insert-new-service";
    }




    @RequestMapping(method = RequestMethod.POST, value = "/insert-new-service")
    public String insertToDatabase (@ModelAttribute(MODEL_REQUEST) Article article, BindingResult result, Model model) {

        LOGGER.info ("inside /insert-new-sevice");
        LOGGER.info ("About to validate user entry");

        validator.validate(article, result);

        if (result.hasErrors()) {
            LOGGER.error("field is empty");
            return "insert-new-service";
        }



        LOGGER.info("About to connect to database");


        Article saved = articleService.saveArticle(article);


        LOGGER.info ("saved data: {}", saved);

        if (saved != null) {

            model.addAttribute("title", article.getTitle());
            model.addAttribute("content", article.getContent());


            LOGGER.info("Database updated; update record: {}", model);

            return "success-page";

        }


        else{

            LOGGER.error("Data is not saved");
            return "insert-new-service";
        }




    }




    //@RequestMapping to edit existing webpage content

    @RequestMapping (method = RequestMethod.GET, value = "/edit-service")
    public String showEditServicePage (Model model) {

        LOGGER.info ("navigated inside edit service page");

        List<Article> articles  = articleService.getAllArticles();

        model.addAttribute("articles", articles);

        return "edit-service";
    }



    @RequestMapping (method = RequestMethod.GET, value = "/retrieve-page")
    public String getArticle(@RequestParam Integer id, Model model){

        LOGGER.info ("navigated inside retrieve-page");
        LOGGER.info ("retrieved article id is: " + id);

        List <Article> retrievedArticle = articleRepository.findByArticleId(id);

        LOGGER.info ("retrieved article: {}", retrievedArticle);

        model.addAttribute("retrievedArticle", retrievedArticle);
        return "retrieve-page";

    }






}
