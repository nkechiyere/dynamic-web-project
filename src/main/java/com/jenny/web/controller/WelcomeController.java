package com.jenny.web.controller;

/**
 * Created by Jennifer Akwari on 12/14/16 12:57 PM.
 */

import com.jenny.web.model.Article;
import com.jenny.web.repository.ArticleRepository;
import com.jenny.web.service.ArticleService;
import com.jenny.web.service.FormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller

public class WelcomeController {

    private static final String MODEL_REQUEST = "request";
    private static Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);

    @Autowired
    private ArticleService articleService;


    @RequestMapping("/")
    public String welcome(Model model){

        List<Article> articles  = articleService.getAllArticles();

        model.addAttribute("articles", articles);

        return "index";
    }



    @RequestMapping (method = RequestMethod.GET, value = "/home")
    public String showHome(Model model) {

        LOGGER.info ("navigated inside home page");

        model.addAttribute("request", new Article());
        return "index";
    }






    @RequestMapping ("/about")
    public String showAbout(Model model) {

        LOGGER.info ("navigated inside about page");

        List<Article> articles  = articleService.getAllArticles();

        model.addAttribute("articles", articles);

        return "about";
    }



}
